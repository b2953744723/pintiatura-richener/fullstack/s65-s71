import { Col, Card, Button } from 'react-bootstrap';
import { useContext } from 'react'
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function PreviewProduct(props) {

	const { user } = useContext(UserContext);
	const { breakPoint, data } = props

	const { _id, name, description, price } = data

	return (
		<Col xs={12} md={breakPoint} className="p-3 mb-5">
			<Card className="cardHighlight CardColor" border="light">
				<Card.Body className="d-flex flex-column">
				  <Card.Title className="text-white">{name}</Card.Title>
				  <Card.Text className="text-start mt-3 text-white">{description}</Card.Text>
				  <div className="mt-auto text-start text-white">₱ {price}</div>
				</Card.Body>
				{
					(user.id === null) ?
					<Button as={Link} to="/login" variant="outline-light" className="ButtonColor text-white">Details</Button>
					:
					<Button as={Link} to={`/products/${_id}`} variant="outline-light" className="ButtonColor text-white">Details</Button>
		        }
			</Card>
		</Col>
	)
};
	            