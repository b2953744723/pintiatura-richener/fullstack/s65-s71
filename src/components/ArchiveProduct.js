import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveCourse({product, isActive, fetchProductData}) {

  const archiveToggle = (productId) => {
    fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}/archive`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })

    .then(res => res.json())
    .then(data => {
      if(data === true) {
        Swal.fire({
          title: 'Success',
          icon: 'success',
          text: 'Product successfully disabled'
        })
        fetchProductData();

      }else {
        Swal.fire({
          title: 'Something Went Wrong',
          icon: 'Error',
          text: 'Please Try again'
        })
        fetchProductData();
      }


    })
  }
    const activateToggle = (productId) => {
    fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}/activate`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })

    .then(res => res.json())
    .then(data => {
      if(data === true) {
        Swal.fire({
          title: 'Success',
          icon: 'success',
          text: 'Product successfully enabled'
        })
        fetchProductData();
      }else {
        Swal.fire({
          title: 'Something Went Wrong',
          icon: 'Error',
          text: 'Please Try again'
        })
        fetchProductData();
      }


    })
  }
 

  return(
    <>
      { isActive ?

        <Button variant="danger" size="sm" onClick={() => archiveToggle(product)}>Archive</Button>
        :
        <Button variant="success" size="sm" onClick={() => activateToggle(product)}>Activate</Button>
      }
    </>

    )
}
