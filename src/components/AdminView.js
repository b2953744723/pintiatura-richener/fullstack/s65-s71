import { useState, useEffect } from 'react';
import { Table, Container, Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct';

export default function AdminView({ productsData, fetchProductData }) {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    const productsArr = productsData.map((product) => {
      return (
        <tr key={product._id}>
          <td>{product.name}</td>
          <td>{product.description}</td>
          <td>{product.price}</td>
          <td className={product.isActive ? "text-success" : "text-danger"}>
            {product.isActive ? "Available" : "Unavailable"}
          </td>
          <td><EditProduct product={product._id} fetchProductData={fetchProductData} /></td>
          <td><ArchiveProduct product={product._id} isActive={product.isActive} fetchProductData={fetchProductData} /></td>
        </tr>
      );
    });

    setProducts(productsArr);
  }, [productsData, fetchProductData]);

  return (
    <>
      <Container>
        <Row className="justify-content-center text-center mb-5">
          <Col>
            <h1 className="text-center my-4 text-white">Admin Dashboard</h1>
            <Button className="mx-3 ButtonColor" variant="outline-light" as={Link} to="/createProduct">Add New Product</Button>
            <Button variant="outline-light" className="ButtonColor" as={Link} to="/orders/getAllOrders">Show Users' Orders</Button>

            <Table striped bordered hover responsive className="mt-5">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Price</th>
                  <th>Availability</th>
                  <th colSpan="2">Actions</th>
                </tr>
              </thead>
              <tbody>
                {products}
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
    </>
  );
};
