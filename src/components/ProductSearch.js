import React, { useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap'
import ProductCard from './ProductCard';

const ProductSearch = () => {
const [searchQuery, setSearchQuery] = useState('');
const [searchResults, setSearchResults] = useState([]);

const handleSearch = async () => {
  try {
    const response = await fetch(`${process.env.REACT_APP_API_URL}/products/search`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ productName: searchQuery })
    });
    const data = await response.json();
    setSearchResults(data);
  } catch (error) {
    console.error('Error searching for courses:', error);
  }
};

  return (
    <Container className="mb-5">
      <Row className="justify-content-center">
        <Col className="p-5">
          <h3 className="text-white">Search:</h3>
          <div className="form-group">
            <input
              type="text"
              id="productName"
              className="form-control"
              value={searchQuery}
              onChange={event => setSearchQuery(event.target.value)}
            />
          </div>
          <button className="ButtonColor text-white mt-3 " variant="outline-light"  onClick={handleSearch}>Search</button>
          <Col>
            <h3 className="text-center text-white">Search Results:</h3>
            <ul>
              {searchResults.map(product => (<ProductCard productProp={product} key={product._id} />))}
            </ul>
          </Col>
        </Col>
      </Row>
    </Container>
  )
};

export default ProductSearch;

