import { useState, useEffect } from 'react'
import { CardGroup } from 'react-bootstrap';
import PreviewProduct from './PreviewProduct';

export default function FeaturedCourses() {

	const [previews, setPreviews] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/allActive`)
		.then(res => res.json())
		.then(data => {

			const numbers = []
			const featured = []

			const generateRandomNums = () => {
				let randomNum = Math.floor(Math.random() * data.length)

				if(numbers.indexOf(randomNum) === -1) {
					numbers.push(randomNum)
				} else {
					generateRandomNums()
				}
			}

			for(let i = 0; i < 6; i++) {
				generateRandomNums()
				featured.push(
					<PreviewProduct data={data[numbers[i]]} key={data[numbers[i]]._id} breakPoint={6}/>
				)
			}

			setPreviews(featured);
		})
	}, [])

	return(
		<>
			<CardGroup>
				{previews}
			</CardGroup>
		</>
	)
}
