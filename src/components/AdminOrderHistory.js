import React, { useEffect, useState, useContext } from 'react';
import { Container, Table, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AdminOrderHistory() {
  const { user } = useContext(UserContext);
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    if (user.isAdmin) {
      fetch(`${process.env.REACT_APP_API_URL}/orders/getAllOrders`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setOrders(data);
        })
        .catch((error) => {
          console.error('Error fetching orders:', error);
        });
    }
  }, [user.isAdmin]);

  return (
    <Container className="mt-5 mb-5">
      {user.isAdmin ? (
        <div className="justify-content-center text-white text-center">
          <h1 className="mb-5">Admin Order History</h1>
          <Table striped bordered hover responsive>
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Product ID</th>
                <th>Quantity</th>
                <th>Total Amount</th>
                <th>Purchased On</th>
              </tr>
            </thead>
            <tbody>
              {orders.map((order, index) => (
                <React.Fragment key={index}>
                  {order.products.map((product, productIndex) => (
                    <tr key={`${index}-${productIndex}`}>
                      <td>{index + 1}</td>
                      <td>{product.name}</td>
                      <td>{product.productId}</td>
                      <td>{product.quantity}</td>
                      <td>{order.totalAmount}</td>
                      <td>{order.purchasedOn}</td>
                    </tr>
                  ))}
                </React.Fragment>
              ))}
            </tbody>
          </Table>
        </div>
      ) : (
        <div>
          <p>Access Denied. You are not authorized to view this page.</p>
        </div>
      )}
      <Button className="mx-3 ButtonColor" variant="outline-light" as={Link} to="/products">Back to dashboard</Button>
    </Container>
  );
}
