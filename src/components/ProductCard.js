import { Row, Col, Card, Button } from 'react-bootstrap';
import { useContext } from 'react'
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function ProductCard({ productProp }) {

  const { user } = useContext(UserContext);

  if (!productProp) {
    return null;
  }

  const { _id, name, description, price } = productProp;

  return (
    <Row className="justify-content-center p-3 mb-5">
      <Col xs={8} md={4}>
        <Card className="cardHighlight CardColor">
          <Card.Body className="d-flex flex-column">
            <Card.Title className="text-white">{name}</Card.Title>
            <Card.Text className="text-white">{description}</Card.Text>
            <Card.Subtitle className="mt-auto text-start text-white">₱ {price}</Card.Subtitle>
          </Card.Body>
          {
            (user.id === null)
            ?
            <Button as={Link} to="/login" variant="outline-light" className="ButtonColor text-white">Details</Button>
            :
            <Button as={Link} to={`/products/${_id}`} variant="outline-light" className="ButtonColor text-white">Details</Button>
          }
        </Card>

      </Col>
    </Row>
  );
}
