import { useContext } from 'react';
import {Navbar, Nav, Container} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavbar() {

	const { user } = useContext(UserContext);

	return (

		<Navbar className="AppColor">
	      <Container fluid >
	        <Navbar.Brand as={Link} to="/" className="justify-content-center text-white">
	        <img 
	        src="https://us.123rf.com/450wm/mmfcreative/mmfcreative2105/mmfcreative210500221/168288452-gs-initial-letter-vector-logo.jpg?ver=6"
             width="40" 
             height="40"
             className="rounded-circle"
	        /></Navbar.Brand>
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">
	          <Nav className="ms-auto py-2 ">
	            <Nav.Link as={NavLink} to="/" className="text-white">Home</Nav.Link>
	            <Nav.Link as={NavLink} to="/products" className="text-white">Product</Nav.Link>
	            {
	              	(user.id !== null) ?
	              	
	              		user.isAdmin 
      					?
      					<>
      						<Nav.Link as={Link} to="/logout" className="text-white">Logout</Nav.Link>
      					</>
      					:
      					<>
      						<Nav.Link as={Link} to="/profile" className="text-white">Profile</Nav.Link>
      						<Nav.Link as={Link} to="/logout" className="text-white">Logout</Nav.Link>
      					</>
	              	:
		              	<>
			              	<Nav.Link as={NavLink} to="/login" className="text-white">Log In</Nav.Link>
			              	<Nav.Link as={NavLink} to="/register" className="text-white">Register</Nav.Link>
			            </>
	            }
	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>
	)
};
