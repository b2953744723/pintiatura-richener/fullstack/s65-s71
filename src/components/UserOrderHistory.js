import React, { useEffect, useState, useContext } from 'react';
import { Container, Table, Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function UserOrderHistory() {
  const { user } = useContext(UserContext);
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    if (!user.isAdmin) {
      fetch(`${process.env.REACT_APP_API_URL}/orders/getOrders`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setOrders(data);
        })
        .catch((error) => {
          console.error('Error fetching user orders:', error);
        });
    }
  }, [user.isAdmin]);

  return (
    <Container className="mt-5 mb-5">
      <Row className="justify-content-center text-center p-3">
        <Col>
          <h1 className="text-white mb-5">Order History</h1>
          <Table striped bordered hover responsive>
            <thead>
              <tr>
                <th>#</th>
                <th>Product Name</th>
                <th>Quantity</th>
                <th>Total Amount</th>
                <th>Purchased On</th>
              </tr>
            </thead>
            <tbody>
              {orders.map((order, index) => (
                <React.Fragment key={index}>
                  {order.products.map((product, productIndex) => (
                    <tr key={`${index}-${productIndex}`}>
                      <td>{index + 1}</td>
                      <td>{product.name}</td>
                      <td>{product.quantity}</td>
                      <td>{order.totalAmount}</td>
                      <td>{order.purchasedOn}</td>
                    </tr>
                  ))}
                </React.Fragment>
              ))}
            </tbody>
          </Table>
          <Button as={Link} to="/profile" variant="outline-light dark" size="md" className="ButtonColor text-white">Back to profile</Button>
        </Col>
      </Row>
    </Container>
  );
}
