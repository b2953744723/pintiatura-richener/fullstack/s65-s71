import React from 'react';

const Footer = () => {
  return (
    <footer fixed="bottom" style={styles.footer}>
      <p className="mt-2 text-white">© {new Date().getFullYear()} Gadget Store. All rights reserved.</p>
    </footer>
  );
};

const styles = {
  footer: {
    backgroundColor: '#2E3F4C',
    textAlign: 'center',
    borderTop: '1px solid #ccc',
    fontSize: '12px',
    position: 'fixed',
    bottom: '0',
    width: '100%',
  },
};

export default Footer;
