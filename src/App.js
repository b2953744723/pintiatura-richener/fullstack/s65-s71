import React from 'react';
import { useState, useEffect } from 'react';
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import AdminView from './components/AdminView';
import AdminOrderHistory from './components/AdminOrderHistory';
import UserOrderHistory from './components/UserOrderHistory';
import UpdateProfile from './components/UpdateProfile';
import ResetPassword from './components/ResetPassword';
import Footer from './components/Footer';
import Home from './pages/Home';
import ProductView from './pages/ProductView';
import AddProduct from './pages/AddProduct';
import CreateOrder from './pages/CreateOrder';
import Product from './pages/Product';
import Profile from './pages/Profile';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Error from './pages/Error';
import './App.css';
import { UserProvider } from './UserContext';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {

  fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
    headers: {
      Authorization: `Bearer ${ localStorage.getItem('token') }`
    }
  })
  .then(res => res.json())
  .then(data => {
    console.log(data)
    
    if (typeof data._id !== "undefined") {

      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      });
    } else {
      setUser({
        id: null,
        isAdmin: null
      });

    }

  })
  }, []);

  useEffect (() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (

    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
          <AppNavbar/>
          <Container fluid>
              <Routes>
                 <Route path="/" element={<Home/>}/>            
                 <Route path="/products" element={<Product/>}/>            
                 <Route path="/register" element={<Register/>}/>
                 <Route path="/createProduct" element={<AddProduct/>}/>
                 <Route path="/products/:productId" element={<ProductView/>}/>
                 <Route path="/products/productId" element={<AdminView/>}/>
                 <Route path="/orders/getAllOrders" element={<AdminOrderHistory/>}/>
                 <Route path="/orders/getOrders" element={<UserOrderHistory/>}/>
                 <Route path="/profile" element={<Profile/>}/>
                 <Route path="/updateProfile" element={<UpdateProfile/>}/>
                 <Route path="/reset-password" element={<ResetPassword/>}/>
                 <Route path="/orders/createOrder" element={<CreateOrder/>}/>
                 <Route path="/login" element={<Login/>}/>
                 <Route path="/logout" element={<Logout/>}/>
                 <Route path="*" element={<Error/>}/>
              </Routes>
          </Container>
          <Footer/>      
      </Router>     
    </UserProvider>

  );
}

export default App;
