import { useState, useEffect } from 'react';
import { Container, Col, Row, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import FeaturedProducts from '../components/FeaturedProducts';


export default function Home(fetchProductData) {

  const [products, setProducts] = useState([]);

  const fetchActiveProductData = () => {
    fetch(`${ process.env.REACT_APP_API_URL }/products/allActive`)
      .then(res => res.json())
      .then(data => {
        setProducts(data);
      });
  };
  useEffect(() => {
    fetchActiveProductData();
  }, []);

  return (
    <>
    <div className="image-container mt-2">
      <img src="https://c0.wallpaperflare.com/path/581/155/730/technology-aesthetic-flatlays-design-c584b6c7a7d7ec0e503d3d02fb9e25c6.jpg" alt="Your Alt Text" />
      <div className="centered-text">
        <h1 className="mb-3">Welcome to Gadget Store</h1>
        <Button as={Link} to="/products" variant="outline-light dark" size="lg" className="ButtonColor text-white">Shop Now</Button>
      </div>
    </div>
    <Container>
      <Row className="justify-content-center text-center mt-5 mb-5">
        <Col>
          <div>
            <h1 className="text-white">Best Seller</h1>
            <FeaturedProducts />
          </div>
        </Col>
      </Row>
    </Container>
    </>
  );
}
