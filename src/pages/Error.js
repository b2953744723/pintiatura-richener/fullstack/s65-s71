import { Button, Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Error() {

	return(
		<Container>
	      <Row className="justify-content-center mt-5">
	        <Col md={4} className="text-center">
	          <div className="error-container">
	            <h1 className="error-heading">Page Not Found</h1>
	            <Button as={Link} to="/" variant="outline-light" className="error-button">Back to Home</Button>
	          </div>
	        </Col>
	      </Row>
	    </Container>
	)
}