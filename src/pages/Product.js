import {useEffect, useState, useContext} from 'react';
import UserContext from '../UserContext';
import UserView from '../components/UserView';
import AdminView from '../components/AdminView';


export default function Product() {

	const { user } = useContext(UserContext);
	const [products, setProducts] = useState([])

	const fetchProductData = () => {

		fetch(`${ process.env.REACT_APP_API_URL }/products/all`)
		.then(res => res.json())
		.then(data => {
			setProducts(data);
		});
	}

	useEffect(() => {
		fetchProductData();
	}, []);

	const fetchActiveProductData = () => {

		fetch(`${ process.env.REACT_APP_API_URL }/products/allActive`)
		.then(res => res.json())
		.then(data => {
			setProducts(data);
		});
	}

	useEffect(() => {
		fetchActiveProductData();
	}, []);

	return (
		<>
			{
				(user.isAdmin === true)
				?
				<AdminView productsData={products} fetchProductData={fetchProductData}/>
				:
				<UserView productsData={products} fetchActiveProductData={fetchActiveProductData}/>
			}
		</>
	)
}