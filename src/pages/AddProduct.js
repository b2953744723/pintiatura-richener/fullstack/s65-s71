import {useState, useContext} from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function AddProduct(){

  const navigate = useNavigate();

  const {user} = useContext(UserContext);
  const [name,setName] = useState("");
  const [description,setDescription] = useState("");
  const [price,setPrice] = useState("");

  function createProduct(e){

    e.preventDefault();

    let token = localStorage.getItem('token');

    fetch(`${ process.env.REACT_APP_API_URL }/products/createProduct`,{
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price
      })
    })
    .then(res => res.json())
    .then(data => {

      if(data){
        Swal.fire({
          icon:"success",
          title: "Item Added"
        })
        navigate("/products");
      } else {
        Swal.fire({
          icon: "error",
          title: "Unsuccessful Product Creation",
          text: data.message
        })
      }
    })
        setName("")
        setDescription("")
        setPrice(0);
  }

  return (

            (user.isAdmin === true)
            ?
            <>
                <Container className="AppColor">
                  <Row className="justify-content-center mt-5 mb-3">
                    <Col md={5} xs={6}>
                      <h1 className="my-5 text-center text-white">Add your Item here</h1>
                      <Form onSubmit={e => createProduct(e)}>
                          <Form.Group>
                              <Form.Label className="text-white my-2">Name:</Form.Label>
                              <Form.Control 
                              type="text" 
                              placeholder="Enter Name" 
                              required 
                              value={name} 
                              onChange={e => {setName(e.target.value)}}/>
                          </Form.Group>
                          <Form.Group>
                              <Form.Label className="text-white my-2">Description:</Form.Label>
                              <Form.Control 
                              type="text" 
                              placeholder="Enter Description" 
                              required 
                              value={description} 
                              onChange={e => {setDescription(e.target.value)}}/>
                          </Form.Group>
                          <Form.Group>
                              <Form.Label className="text-white my-2">Price:</Form.Label>
                              <Form.Control 
                              type="number" 
                              placeholder="Enter Price" 
                              required 
                              value={price} 
                              onChange={e => {setPrice(e.target.value)}}/>
                          </Form.Group>
                          <Button className="mx-3 ButtonColor" variant="outline-light" as={Link} to={"/products"}>Back to Dashboard</Button>
                          <Button variant="outline-light" type="submit" className="my-5 ButtonColor">Submit</Button>
                      </Form>
                    </Col>
                  </Row>
                </Container>
        </>
            :
            <Navigate to="/products" />
  )


}
