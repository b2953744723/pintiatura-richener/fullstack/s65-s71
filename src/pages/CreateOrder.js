import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate } from 'react-router-dom';
import Swal from "sweetalert2";
import UserContext from '../UserContext';

export default function ProductView() {

  const { productId } = useParams()
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [quantity, setQuantity] = useState(1);
  const [price, setPrice] = useState(0);
  const [totalAmount, setTotalAmount] = useState(0);

  useEffect(() => {
    console.log(productId)

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
    .then(res => res.json())
    .then(data => {

      setName(data.name);
      setDescription(data.description);
      setPrice(data.price);
    })
  }, [productId])

  const handleQuantityIncrease = () => {
      setQuantity((prevQuantity) => prevQuantity + 1);
    };

    const handleQuantityDecrease = () => {
      setQuantity((prevQuantity) => Math.max(prevQuantity - 1, 1));
    };

    useEffect(() => {
      // Fetch product details from the server using the productId
      fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
        .then((res) => res.json())
        .then((data) => {
          console.log(data)
          setName(data.name);
          setDescription(data.description);
          setPrice(data.price);
        })
        .catch((error) => {
          console.error('Error fetching product details:', error);
        });
    }, [productId]);

    useEffect(() => {
      // Calculate the total amount whenever quantity or price changes
      setTotalAmount(price * quantity);
    }, [price, quantity]);

    const handleSubmitOrder = (e) => {
        e.preventDefault();

        if (user.isAdmin) {
          Swal.fire({
            title: 'Cannot buy as an Admin',
            icon: 'error',
            text: 'Admins cannot make a purchase.',
          });
          return;
        }
        if (quantity <= 0) {
          Swal.fire({
            title: 'Invalid Quantity',
            icon: 'error',
            text: 'Quantity must be greater than 0.',
          });
          return;
        }
        fetch(`${process.env.REACT_APP_API_URL}/orders/createOrder`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`,
          },
          body: JSON.stringify({
            productId: productId, 
            quantity: quantity,
          }),
        })
          .then((res) => {
            if (!res.ok) {
              throw new Error('Failed to place the order');
            }
            return res.json();
          })
          .then((data) => {
            Swal.fire({
              title: 'Order Placed Successfully',
              icon: 'success',
              text: 'Your order has been placed successfully.',
            });
            navigate('/products');
          })
          .catch((error) => {
            console.error('Error placing the order:', error);
            Swal.fire({
              title: 'Error',
              icon: 'error',
              text: 'An error occurred while placing the order. Please try again later.',
            });
          });
      };

    return (
      <Container className="mt-5">
        <Row>
          <Col lg={{ span: 6, offset: 3 }}>
            <Card>
              <Card.Body className="text-start">
                <>
                  <Card.Title className="text-center">{name}</Card.Title>
                  <Card.Subtitle>{description}</Card.Subtitle>
                  <Card.Subtitle>Price: {price}</Card.Subtitle>
                  <Card.Subtitle>Quantity:</Card.Subtitle>
                  <Button
                    variant="secondary"
                    onClick={handleQuantityDecrease}
                    disabled={quantity <= 1}
                    style={{ color: 'black', fontSize: '14px', padding: '5px 10px' }}
                  >
                    -
                  </Button>
                  <span>{quantity}</span>
                  <Button
                    variant="secondary"
                    onClick={handleQuantityIncrease}
                    style={{ color: 'black', fontSize: '14px', padding: '5px 10px' }}
                  >
                    +
                  </Button>
                  <Card.Subtitle>Total Amount: {totalAmount}</Card.Subtitle>
                </>
              </Card.Body>
              <Button variant="primary" onClick={handleSubmitOrder} className="w-100">
                Checkout
              </Button>
            </Card>
          </Col>
        </Row>
      </Container>

    );
}