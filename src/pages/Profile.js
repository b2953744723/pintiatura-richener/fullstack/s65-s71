import { useState, useContext, useEffect } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import UpdateProfile from '../components/UpdateProfile';
import ResetPassword from '../components/ResetPassword';

export default function Profile() {
  const { user } = useContext(UserContext);

  const [details, setDetails] = useState({});

  useEffect(() => {
    fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (typeof data._id !== 'undefined') {
          setDetails(data);
        }
      });
  }, []);

  const handleProfileUpdate = (updatedProfileData) => {
    setDetails(updatedProfileData);
  };

  const handlePasswordReset = () => {
      Swal.fire({
        title: 'Successful',
        icon: 'success',
        text: 'You successfully reset your password.',
      })
    };

  return (
    <>
      {user.id === null ? (
        <Navigate to="/" />
      ) : (
        <>
          <Container>
            <Row className="text-center mt-5" style={{ backgroundColor: '#4B636D' }}>
              <Col className="p-5 text-white">
                <hr />
                <h2 className="mt-3">{`${details.firstName} ${details.lastName}`}</h2>
                <hr />
                <h4 >Contacts</h4>
                <ul className="remove-bullets">
                  <li className="text-white">Email: {details.email}</li>
                  <li className="text-white">Mobile No: {details.mobileNo}</li>
                </ul>
              </Col>
            </Row>
          </Container>

          <Container>
            <Row className="pt-4 mt-4">
              <Col className="d-flex justify-content-center">
                <div style={{ marginRight: '20px' }}>
                  <UpdateProfile onProfileUpdate={handleProfileUpdate} />
                </div>
                <div style={{ marginRight: '20px' }}>
                  <ResetPassword onPasswordReset={handlePasswordReset}/>
                </div>
                <div >
                  <Button size="sm" as={Link} to="/orders/getOrders" variant="outline-light" className="ButtonColor text-white">Order History</Button>
                </div>
              </Col>
            </Row>
          </Container>
        </>
      )}
    </>
  );
}
