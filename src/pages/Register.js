import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'

export default function Register() {

	const {user} = useContext(UserContext);

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [ isActive, setIsActive] = useState(false);

	function registerUser(e) {

		e.preventDefault();

		fetch(`${ process.env.REACT_APP_API_URL }/users/register`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data) {

				setFirstName("");
				setLastName("");
				setEmail("");
				setMobileNo("");
				setPassword("");
				setConfirmPassword("");

				Swal.fire({
					title: "Successfully Register",
					icon: "success",
					text: "Welcome to Gadget Store!"
				});

			} else {

				Swal.fire({
					title: "Error",
					icon: "error",
					text: "Please try again later"
				});
			}

		})
	};

	useEffect(() => {
	 
	    if (
	      firstName !== "" &&
	      lastName !== "" &&
	      email !== "" &&
	      mobileNo !== "" &&
	      password !== "" &&
	      confirmPassword !== "" &&
	      password === confirmPassword &&
	      mobileNo.length === 11
	    ) {
	      setIsActive(true);
	    } else {
	      setIsActive(false);
	    }
	  }, [firstName, lastName, email, mobileNo, password, confirmPassword]);



	return (
		
		(user.id !== null) ?
		    <Navigate to="/products" />
		:
		<Container className="mt-5 mb-5" style={{ backgroundColor: '#4B636D' }}>
			<Row className="justify-content-center">
				<Col md={4} xs={6} className="mt-5 mb-5">
					<Form onSubmit={e => registerUser(e)}>
					  <h1 className="my=5 text-center text-white">Register</h1>

					  <Form.Group className="mb-3" controlId="First Name">
					    <Form.Label className="text-white">First Name</Form.Label>
					    <Form.Control 
					    	type="text" 
					    	placeholder="Enter First Name" 
					    	required
					    	value={firstName}
					    	onChange={e => {setFirstName(e.target.value)}}
					    />
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="Last Name">
					    <Form.Label className="text-white">Last Name</Form.Label>
					    <Form.Control 
					    	type="text" 
					    	placeholder="Enter Last Name" 
					    	required 
					    	value={lastName}
					    	onChange={e => {setLastName(e.target.value)}}
					    />
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="Email address">
					    <Form.Label className="text-white">Email address</Form.Label>
					    <Form.Control 
					    	type="email" 
					    	placeholder="name@example.com"  
					    	required
					    	value={email}
					    	onChange={e => {setEmail(e.target.value)}}
					    />
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="Mobile No">
					    <Form.Label className="text-white">Mobile No:</Form.Label>
					    <Form.Control 
					    	type="text" 
					    	placeholder="Enter 11 Digit No."
					    	required 
					    	value={mobileNo}
					    	onChange={e => {setMobileNo(e.target.value)}}
					    />
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="Password1">
					    <Form.Label className="text-white">Password</Form.Label>
					    <Form.Control 
					    	type="password" 
					    	placeholder="Enter Password" 
					    	required 
					    	value={password}
					    	onChange={e => {setPassword(e.target.value)}}
					    />
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="Password2">
					    <Form.Label className="text-white">Confirm Password:</Form.Label>
					    <Form.Control 
					    	type="password" 
					    	placeholder="Confirm Password" 
					    	required 
					    	value={confirmPassword}
					    	onChange={e => {setConfirmPassword(e.target.value)}}
					    />
					  </Form.Group>

					  <Form.Group className="mb-3 text-center" controlId="submitBtn">
					    {
					      isActive 
					        ? <Button variant="primary" type="submit" className="w-100">Submit</Button>
					        : <Button variant="danger" type="submit" className="w-100" disabled>Submit</Button>
					    }
					  </Form.Group>
					  <p className="text-center text-white mt-4">
					    Already have an account? <Link to="/login" className="colorLink">Click here</Link> to log in.
					  </p>
					</Form>
				</Col>
			</Row>
		</Container>
	)
};
