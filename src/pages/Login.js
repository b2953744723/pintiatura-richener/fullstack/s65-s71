import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

	const { user, setUser } = useContext(UserContext);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [ isActive, setIsActive] = useState(false);

	function authenticateUser(e) {
		e.preventDefault();

		fetch(`${ process.env.REACT_APP_API_URL }/users/login`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data.access) {
				localStorage.setItem("token", data.access);
				retrieveUserDetails(data.access);
				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Gadget Store!"
				});
			} else {
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your details and try again!"
				});
			}
		})
		setEmail('');
		setPassword('');
	};

	const retrieveUserDetails = (token) => {

		fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			});
		})
	}

	useEffect(() => {
		if(email !== "" && password !== "") {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

	return(

		(user.id !== null) ?
			<Navigate to="/" />
			:
			<Container className="mt-5" style={{ backgroundColor: '#4B636D' }}>
				<Row className="justify-content-center">
					<Col md={4} xs={6} className="mt-5">
						<Form onSubmit={e => authenticateUser(e)}>
						  <h1 className="my=5 text-center text-white">Login</h1>

						  <Form.Group className="mb-3" controlId="Email address">
						    <Form.Label className="text-white">Email address</Form.Label>
						    <Form.Control 
						    	type="email" 
						    	placeholder="name@example.com"  
						    	required
						    	value={email}
						    	onChange={e => {setEmail(e.target.value)}}
						    />
						  </Form.Group>

						  <Form.Group className="mb-3" controlId="Password1">
						    <Form.Label className="text-white">Password</Form.Label>
						    <Form.Control 
						    	type="password" 
						    	placeholder="Enter Password" 
						    	required 
						    	value={password}
						    	onChange={e => {setPassword(e.target.value)}}
						    />
						  </Form.Group>
						  <Form.Group className="mb-3 text-center" controlId="submitBtn">
						    {
						      isActive 
						        ? <Button variant="primary" type="submit" className="w-100">Submit</Button>
						        : <Button variant="danger" type="submit" className="w-100" disabled>Submit</Button>
						    }
						  </Form.Group>
						  <p className="text-center text-white mt-4 mb-5">
						    Don't have an account yet? <Link to="/register" className="colorLink"> Click here </Link> to register.
						  </p>
						</Form>
					</Col>
				</Row>
			</Container>
	)
};